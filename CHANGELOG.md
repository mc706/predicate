# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).



## Unreleased
---

### New

### Changes

### Fixes

### Breaks


## 0.3.0 - (2020-05-01)
---

### New
* add `__type` comparator


## 0.2.0 - (2020-05-01)
---

### New
* add `nand` combinator


## 0.1.1 - (2020-05-01)
---

### Changes
* Updating documentation 
* Increasing test coverage


## 0.1.0 - (2020-04-30)
---

### New
* Initial setup of predicate functions
