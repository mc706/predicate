import unittest

from predicate.utils import safe_lens


class SafeLensTestCase(unittest.TestCase):
    def test_nested_dict(self):
        data = {
            'foo': {
                'bar': {
                    'baz': 'bizz'
                }
            }
        }
        getter = safe_lens(['foo', 'bar', 'baz'])
        self.assertEqual('bizz', getter(data))

    def test_can_be_used_more_than_once(self):
        data = {
            'foo': {
                'bar': {
                    'baz': 'bizz'
                }
            }
        }
        getter = safe_lens(['foo', 'bar', 'baz'])
        self.assertEqual('bizz', getter(data))
        self.assertEqual('bizz', getter(data))

    def test_nested_class(self):

        class Bar:
            def __init__(self, baz):
                self.baz = baz

        class Foo:
            def __init__(self, bar):
                self.bar = bar

        bar = Bar("bizz")
        data = Foo(bar)
        getter = safe_lens(['bar', 'baz'])
        self.assertEqual('bizz', getter(data))

    def test_nested_mixed(self):
        class Foo:
            def __init__(self, bar):
                self.bar = bar

        data = Foo({'baz': 'bizz'})
        getter = safe_lens(['bar', 'baz'])
        self.assertEqual('bizz', getter(data))

    def test_invalid_path(self):
        data = {
            'foo': {
                'bar': {
                    'baz': 'bizz'
                }
            }
        }
        getter = safe_lens(['foo', 'bar', 'foo'])
        self.assertEqual(None, getter(data))
