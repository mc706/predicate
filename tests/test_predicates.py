import unittest

from predicate.predicates import _process_predicate, _process_predicate_field, kwargs_to_predicates
from predicate.comparator import Comparator


class ProcessPredicateFieldTestCase(unittest.TestCase):
    def test_no_underscores(self):
        getter, comparator = _process_predicate_field('name')
        self.assertEqual('bob', getter({'name': 'bob'}))
        self.assertEqual(Comparator.EQ, comparator)

    def test_with_comparator(self):
        getter, comparator = _process_predicate_field('name__exact')
        self.assertEqual('bob', getter({'name': 'bob'}))
        self.assertEqual(Comparator.EXACT, comparator)

    def test_invalid_comparator(self):
        getter, comparator = _process_predicate_field('name__unknown')
        self.assertEqual(None, getter({'name': 'bob'}))
        self.assertEqual(Comparator.EQ, comparator)

    def test_nested_gets(self):
        getter, comparator = _process_predicate_field('name__first__iexact')
        self.assertEqual('bob', getter({'name': {'first': 'bob'}}))
        self.assertEqual(Comparator.IEXACT, comparator)


class ProcessPredicateTestCase(unittest.TestCase):
    def test_predicate_can_be_used_more_than_once(self):
        func = _process_predicate('name', 'bob')
        self.assertTrue(func({'name': 'bob'}))
        self.assertTrue(func({'name': 'bob'}))

    def test_equals_predicate(self):
        func = _process_predicate('name', 'bob')
        self.assertTrue(func({'name': 'bob'}))
        self.assertFalse(func({'name': 'joe'}))

    def test_greater_than_predicate(self):
        func = _process_predicate('age__gt', 3)
        self.assertTrue(func({'age': 4}))
        self.assertFalse(func({'age': 3}))
        self.assertFalse(func({'age': 2}))

    def test_gte(self):
        func = _process_predicate('age__gte', 3)
        self.assertTrue(func({'age': 4}))
        self.assertTrue(func({'age': 3}))
        self.assertFalse(func({'age': 2}))

    def test_lt(self):
        func = _process_predicate('age__lt', 3)
        self.assertFalse(func({'age': 4}))
        self.assertFalse(func({'age': 3}))
        self.assertTrue(func({'age': 2}))

    def test_lte(self):
        func = _process_predicate('age__lte', 3)
        self.assertFalse(func({'age': 4}))
        self.assertTrue(func({'age': 3}))
        self.assertTrue(func({'age': 2}))

    def test_ne(self):
        func = _process_predicate('age__ne', 3)
        self.assertTrue(func({'age': 4}))
        self.assertFalse(func({'age': 3}))

    def test_func(self):
        func = _process_predicate('age__func', lambda x: x % 2 == 0)
        self.assertTrue(func({'age': 4}))
        self.assertFalse(func({'age': 5}))

    def test_contains(self):
        func = _process_predicate('name__contains', 'ill')
        self.assertTrue(func({'name': 'Bill'}))
        self.assertFalse(func({'name': 'Bob'}))
        func = _process_predicate('tags__contains', 'new')
        self.assertTrue(func({'tags': ['sale', 'new']}))
        self.assertFalse(func({'tags': ['sale', 'old']}))

    def test_startswith(self):
        func = _process_predicate('name__startswith', 'smi')
        self.assertTrue(func({'name': 'smith'}))
        self.assertFalse(func({'name': 'Bob'}))

    def test_endswith(self):
        func = _process_predicate('name__endswith', 'ith')
        self.assertTrue(func({'name': 'ith'}))
        self.assertFalse(func({'name': 'Bob'}))

    def test_exact(self):
        func = _process_predicate('name__exact', 'bill')
        self.assertTrue(func({'name': 'bill'}))
        self.assertFalse(func({'name': 'ith'}))

    def test_iexact(self):
        func = _process_predicate('name__iexact', 'bill')
        self.assertTrue(func({'name': 'Bill'}))
        self.assertFalse(func({'name': 'bob'}))

    def test_in(self):
        func = _process_predicate('name__first__in', ['Bill', 'Joe'])
        self.assertTrue(func({'name': {'first': 'Joe'}}))
        self.assertFalse(func({'name': {'first': 'Jane'}}))

    def test_type(self):
        func = _process_predicate('age__type', int)
        self.assertTrue(func({'age': 18}))
        self.assertFalse(func({'age': 18.0}))

class ProcessKwargsTestCase(unittest.TestCase):
    def test_single(self):
        predicates = kwargs_to_predicates({'name__first__iexact': 'John'})
        func = predicates[0]
        self.assertTrue(func({'name': {'first': 'john'}}))

    def test_two(self):
        predicates = kwargs_to_predicates({'name__first__iexact': 'John', 'name__last__iexact': 'Smith'})
        for predicate in predicates:
            self.assertTrue(predicate({'name': {'first': 'john', 'last': 'smith'}}))
