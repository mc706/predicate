import unittest

from predicate import where


class WhereTestCase(unittest.TestCase):
    def test_single_predicate(self):
        func = where(age__gte=18)
        self.assertTrue(func({'age': 20}))
        self.assertFalse(func({'age': 10}))

    def test_compound_predicate(self):
        func = where(name__first__startswith='J', age__gte=20)
        self.assertTrue(func({'name': {'first': 'John'}, 'age': 25}))
        self.assertFalse(func({'name': {'first': 'Bob'}, 'age': 25}))

    def test_impossible_predicate_set(self):
        func = where(age__lt=30, age__gt=30)
        self.assertFalse(func({'age': 20}))
        self.assertFalse(func({'age': 30}))
        self.assertFalse(func({'age': 40}))

    def test_combinator_or(self):
        func = where(age__lt=30, age__gt=30, combinator='or')
        self.assertTrue(func({'age': 20}))
        self.assertFalse(func({'age': 30}))
        self.assertTrue(func({'age': 40}))

    def test_combinator_explict_and(self):
        func = where(age__lt=30, age__gt=30, combinator='and')
        self.assertFalse(func({'age': 20}))
        self.assertFalse(func({'age': 30}))
        self.assertFalse(func({'age': 40}))

    def test_combinator_none(self):
        func = where(age__lt=30, age__gt=30, combinator='none')
        self.assertFalse(func({'age': 20}))
        self.assertTrue(func({'age': 30}))
        self.assertFalse(func({'age': 40}))

    def test_invalid_combinator(self):
        with self.assertRaises(ValueError):
            where(age=18, combinator='invalid')

    def test_custom_combinator(self):
        func = where(age__lt=30, age__gt=30, combinator=lambda xs: sum(xs) == 1)
        self.assertTrue(func({'age': 20}))
        self.assertFalse(func({'age': 30}))
        self.assertTrue(func({'age': 40}))

    def test_nand_combinator(self):
        func = where(age=20, gender='F', combinator='nand')
        self.assertTrue(func({'age': 19, 'gender': 'F'}))
        self.assertTrue(func({'age': 19, 'gender': 'M'}))
        self.assertFalse(func({'age': 20, 'gender': 'F'}))
