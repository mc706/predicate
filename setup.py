import os
from setuptools import setup, find_packages

from src.predicate import __version__ as v

version = os.getenv("MODULE_VERSION_ID", v)

setup(
    name='predicate',
    description='Predicate Functions for Python in the style of django filters',
    long_description=open('README.md', 'r').read(),
    long_description_content_type='text/markdown',
    version=version,
    author='Ryan McDevitt',
    author_email='mcdevitt.ryan@gmail.com',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    url='https://gitlab.com/mc706/predicate',
    install_requires=[],
    extras_require={'dev': ['prospector', 'coverage', 'mypy', 'pytest', 'pytest-cov']},
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Software Development :: Libraries',
        'Topic :: Utilities',
    ],
    project_urls={
        'Docs': 'https://predicate.readthedocs.io/en/stable/#',
        'Source': 'https://gitlab.com/mc706/predicate',
        'Bug Reports/Issues': 'https://gitlab.com/mc706/predicate/issues'
    },
)
