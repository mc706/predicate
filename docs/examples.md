Here are some examples of putting it all together:

```python 
>>> from predicate import where

>>> people = [
...     { 
...         'name': {
...             'first': 'John', 
...             'last': 'Smith',
...         }, 
...         'age': 32, 
...         'employment': [
...             {'name': 'McDonalds', 'position': 'Manager'},
...             {'name': 'CSV', 'position': 'Cashier'}
...         ]
...     },
...     { 
...         'name': {
...             'first': 'Jane', 
...             'last': 'Smith', 
...         },
...         'age': 30, 
...         'employment': [
...             {'name': 'BurgerKing', 'position': 'Manager'}
...         ]
...     },
...     {
...         'name': {
...             'first': 'Billy', 
...             'last': 'Bob',
...         }, 
...         'age': 55, 
...         'employment': [
...             {'name': 'Microsoft', 'position': 'Programmer'}
...         ]
...     },
...     {
...         'name': {
...             'first': 'Jill', 
...             'last': 'Jones', 
...         },
...         'age': 21, 
...         'employment': []
...     },
... ]

>>> smiths = list(filter(where(name__last__iexact='smith'), people))

>>> smiths
[{'name': {'first': 'John', 'last': 'Smith'}, 'age': 32, 'employment': [{'name': 'McDonalds', 'position': 'Manager'}, {'name': 'CSV', 'position': 'Cashier'}]}, {'name': {'first': 'Jane', 'last': 'Smith'}, 'age': 30, 'employment': [{'name': 'BurgerKing', 'position': 'Manager'}]}]

```
