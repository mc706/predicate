# Welcome to Predicates Documentation

<table>
    <tr>
        <th>Documentation:</th>
        <td><a href="https://predicate.readthedocs.io/">https://predicate.readthedocs.io/</a></td>
    </tr>
    <tr>
        <th>Source Code:</th>
        <td><a href="https://gitlab.com/mc706/predicate">https://gitlab.com/mc706/predicate</a></td>
    </tr>
    <tr>
        <th>Issue Tracker:</th>
        <td><a href="https://gitlab.com/mc706/predicate/issues">https://gitlab.com/mc706/predicate/issues</a></td>
    </tr>
    <tr>
        <th>PyPI:</th>
        <td><a href="https://pypi.org/project/predicate/">https://pypi.org/project/predicate/</a></td>
    </tr>
</table>

Predicate takes the django queryset filtering language and applies it to building python native predicate functions that
you can pass to the `filter` builtin. 


This library is mean to make creating native python filter functions readable and easy:


```python 
>>> from predicate import where

>>> people = [
...     {'name': {'first': 'Joe', 'last': 'Smith'}, 'age': 25},
...     {'name': {'first': 'Jane', 'last': 'Smith'}, 'age': 27},
...     {'name': {'first': 'John', 'last': 'Smith'}, 'age': 33},
...     {'name': {'first': 'Bill', 'last': 'Bob'}, 'age': 40},
...     {'name': {'first': 'Joe', 'last': 'Bob'}, 'age': 59},
... ]


>>> young_smiths = list(filter(where(name__last__iexact='smith', age__lte=30), people))

>>> young_smiths
[{'name': {'first': 'Joe', 'last': 'Smith'}, 'age': 25}, {'name': {'first': 'Jane', 'last': 'Smith'}, 'age': 27}]

```

## Installation
This project is distributed via `pip`. To get started:

```
pip install predicate
```
